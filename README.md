# An implementation of the JSON-RPC for Scheme


## Introduction

*EXPERIMENTAL*

The JSON-RPC is allows calling procedures on remote servers by
exchanging JSON objects. It's specification can be found at
[https://www.jsonrpc.org/specification](https://www.jsonrpc.org/specification).

The low-level API strives to be R7RS compliant, relying on some SRFI's when needed. 
So far it was only tested under CHICKEN 5 and Guile 3.

## Installing

The library is small enough so that simply *loading* the *.sld* file
and *importing* `(json-rpc)` should be sufficient. 
Due to limitations of Guile's R7RS support, a separate *.sld* is
provided for it.

## Documentation

See [http://wiki.call-cc.org/eggref/5/json-rpc](http://wiki.call-cc.org/eggref/5/json-rpc).

## LICENSE

MIT License

Copyright (c) 2021 Ricardo G. Herdt

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
