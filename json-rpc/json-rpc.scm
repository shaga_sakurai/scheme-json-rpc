
(define last-id (make-parameter 0))

(define (increment-last-id!)
  (last-id (+ 1 (last-id))))

(define (json-rpc-call in-port out-port method params)
  (json-rpc-write `((id . ,(last-id))
                    (method . ,method)
                    (params . ,params))
                  out-port)
  (increment-last-id!)
  (let ((response (json-rpc-read in-port)))
    (cond ((list? response)
           (let ((result (assoc 'result response))
                 (err (assoc 'error response)))
             (cond (result => cdr)
                   (err (error (cdr err)))
                   (else (error "invalid response: " err)))))
          (else response))))

(define (json-rpc-call/tcp tcp-address tcp-port method params)
  (define-values (in-port out-port) (tcp-connect tcp-address tcp-port))
  (define res (json-rpc-call in-port out-port method params))
  (close-output-port out-port)
  (close-input-port in-port)
  res)

(define (json-rpc-start-server/tcp tcp-port)
  (parameterize ((tcp-read-timeout #f))
    (let ((listener (tcp-listen tcp-port)))
      (let loop ()
        (let-values (((in-port out-port)
                      (tcp-accept listener)))
          (if (eqv? (json-rpc-loop in-port out-port) 'json-rpc-exit)
              (begin
                (close-input-port in-port)
                (close-output-port out-port)
                (tcp-close listener))
              (loop)))))))


