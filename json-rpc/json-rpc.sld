(define-library (json-rpc)

(export json-rpc-call
        json-rpc-call/tcp
        json-rpc-log-level
        json-rpc-exit
        json-rpc-handler-table
        json-rpc-start-server/tcp)

(import (chicken tcp)
        (json-rpc lolevel)
        (medea)
        (r7rs)
        (scheme base)
        (scheme char))

(include "json-rpc.scm"))
