(define-module (json-rpc lolevel)

#:export (json-rpc-exit
          json-rpc-handler-table
          json-rpc-log-level
          json-rpc-loop
          json-rpc-read
          json-rpc-write

          custom-error-codes
          make-json-rpc-custom-error
          make-json-rpc-internal-error
          make-json-rpc-invalid-request-error
          json-rpc-error?
          json-rpc-custom-error?
          json-rpc-internal-error?
          json-rpc-invalid-request-error?)

#:use-module (scheme base)
#:use-module (scheme char)
#:use-module (srfi srfi-1)
#:use-module (srfi srfi-13)
#:use-module (srfi srfi-28)
#:use-module (srfi srfi-69)
#:use-module (srfi srfi-145)
#:use-module (srfi srfi-180)

#:declarative? #f)

(include "lolevel.scm")
